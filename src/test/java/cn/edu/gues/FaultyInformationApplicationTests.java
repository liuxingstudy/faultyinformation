package cn.edu.gues;

import cn.edu.gues.pojo.FaultyTable;
import cn.edu.gues.service.AdminService;
import cn.edu.gues.service.Impl.AdminServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class FaultyInformationApplicationTests {

    @Test
    void contextLoads() {
        AdminService adminService=new AdminServiceImpl();
        List<FaultyTable> faultyTables = adminService.adminList();
        for (FaultyTable table : faultyTables) {
            System.out.println(table);
        }
    }

}
