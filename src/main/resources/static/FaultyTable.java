package com.sample;


public class FaultyTable {

  private long faultyid;
  private String name;
  private String pass;
  private String work;
  private long age;
  private String sex;
  private String nation;
  private String zzmm;
  private java.sql.Date birth;
  private String xueli;
  private String hun;


  public long getFaultyid() {
    return faultyid;
  }

  public void setFaultyid(long faultyid) {
    this.faultyid = faultyid;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getPass() {
    return pass;
  }

  public void setPass(String pass) {
    this.pass = pass;
  }


  public String getWork() {
    return work;
  }

  public void setWork(String work) {
    this.work = work;
  }


  public long getAge() {
    return age;
  }

  public void setAge(long age) {
    this.age = age;
  }


  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }


  public String getNation() {
    return nation;
  }

  public void setNation(String nation) {
    this.nation = nation;
  }


  public String getZzmm() {
    return zzmm;
  }

  public void setZzmm(String zzmm) {
    this.zzmm = zzmm;
  }


  public java.sql.Date getBirth() {
    return birth;
  }

  public void setBirth(java.sql.Date birth) {
    this.birth = birth;
  }


  public String getXueli() {
    return xueli;
  }

  public void setXueli(String xueli) {
    this.xueli = xueli;
  }


  public String getHun() {
    return hun;
  }

  public void setHun(String hun) {
    this.hun = hun;
  }

}
