package cn.edu.gues.mapper;

import cn.edu.gues.pojo.FaultyTable;
import cn.edu.gues.pojo.UserTable;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AdminMapper {
    UserTable adminLogin(UserTable userTable);

    List<FaultyTable> adminList();

    int changePass(UserTable userTable);

    FaultyTable selectUserById(int faultyid);

    void fildIdDel(int faultyid);

    int faultyAdd(FaultyTable faultyTable);
}


