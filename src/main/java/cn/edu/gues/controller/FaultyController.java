//package cn.edu.gues.controller;
//
//import cn.edu.gues.pojo.FaultyTable;
//import cn.edu.gues.service.AdminService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//@Controller
//public class FaultyController {
//    @Autowired
//    AdminService adminService;
//    //跳转到faulty-add页面
//    @RequestMapping(name = "add",value = "/admin/faulty-add.html")
//    String add( FaultyTable faultyTable){
//        int i = adminService.faultyAdd(faultyTable);
//        if (i>0){
//            return "/admin/faulty-list";
//        }
//        return "/admin/faulty-add";
//    }
//}
