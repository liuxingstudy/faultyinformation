package cn.edu.gues.controller;

import cn.edu.gues.pojo.FaultyTable;
import cn.edu.gues.pojo.UserTable;
import cn.edu.gues.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class AdminController {
    @Autowired
    private AdminService adminService;

    //    登录
    @RequestMapping(name = "toAdmin", value = "admin/admin-login.html")//跳转到管理员登录界面
    public String toAdmin() {
        return "admin/admin-login";
    }

    @RequestMapping(name = "adminLogin", value = "adminLogin")//登录验证
    public String adminToLogin(Model model, String name, String pass, HttpSession session) {

        UserTable userTable = new UserTable();
        userTable.setName(name);
        userTable.setPass(pass);
        userTable = adminService.adminLogin(userTable);

        if (userTable == null) {
            model.addAttribute("cantLoginMsg", "账号或密码错误");
            return "admin/admin-login";
        } else {
            session.setAttribute("name", userTable.getName());
            return "redirect:admin/admin-index.html";
        }

    }

    @RequestMapping(name = "toIndex", value = "admin/admin-index.html")//登陆成功后跳转主界面
    public String toIndex() {
        return "admin/admin-index";
    }

    //删除职工
    @RequestMapping ("/del")
    String tDelete(int faultyid){
        adminService.fildIdDel(faultyid);
        return "redirect:/admin/faulty-list.html";
    }

    //    退出界面
    @RequestMapping("/exit")
    String exitAll(HttpSession session){
        session.invalidate();
        return "admin/admin-login";
    }
    //跳转到faulty-list页面
    @RequestMapping(name = "adminList", value = "admin/faulty-list.html")
    public String adminToList(Model model) {
        List<FaultyTable> faultyTables = adminService.adminList();
        model.addAttribute("faultyTables", faultyTables);
        return "/admin/faulty-list";
    }
    //跳转到faulty-add页面
//    @RequestMapping(name = "add",value = "/admin/faulty-add.html")
//    String add( FaultyTable faultyTable){
//        int i = adminService.faultyAdd(faultyTable);
//        if (i>0){
//            return "/admin/faulty-list";
//        }
//        return "/admin/faulty-add";
//    }

    @RequestMapping("/admin/faulty-add")
    public String faultyAddList(FaultyTable faultyTable) {
        int i = adminService.faultyAdd(faultyTable);
        if (i>0){
            return "/admin/faulty-list";
        }
        else {
            return "/admin/faulty-add";
        }

    }
    //跳转到修改密码界面
    @RequestMapping("admin/change-password.html")
    public String toChangePassword() {
        return "admin/change-password";
    }

    //验证密码是否正确
    @RequestMapping("/changePassword")
    public String changePassword(String oldPassword, String newPassword, HttpSession session, Model model) {
        String name = (String) session.getAttribute("name");
        UserTable userTable = new UserTable();
        userTable.setName(name);
        userTable.setPass(oldPassword);
        userTable = adminService.adminLogin(userTable);
        if (userTable == null) {
            return "redirect:cantChangPass";
        } else {
            userTable.setPass(newPassword);
            int i = adminService.changePass(userTable);
            if (i != 0) {
                model.addAttribute("successChangPass", "修改成功，请重新登录");
                session.removeAttribute("name");
                return "redirect:/successChangPass";
            } else {
                return "redirect:/cantChangPass";
            }
        }
    }

    //如密码正确，则在这里跳转登录界面
    @RequestMapping(name = "successChangPass", value = "/successChangPass")
    public String successChangPass(Model model) {
        model.addAttribute("successChangPass", "修改成功，请重新登录");
        return "admin/admin-login";
    }

    //如密码错误，则在这里跳转修改密码界面
    @RequestMapping(name = "cantChangPass", value = "/cantChangPass")
    public String cantChangPass(Model model) {
        model.addAttribute("cantChangPass", "密码不正确");
        return "admin/change-password";
    }

    //利用id查询用户信息
    @RequestMapping(name = "toEditInfoPage",value = "/toEditInfoPage")
    public String toEditInfoPage(Model model,String faultyid){
        FaultyTable faultyTable = adminService.selectUserById(Integer.parseInt(faultyid));
        model.addAttribute("faultyTable",faultyTable);
        return "admin/faulty-edit";
    }
}
