package cn.edu.gues.pojo;


public class YuanTable {

    private long yuanid;
    private String yuanname;
    private long yuancount;


    public long getYuanid() {
        return yuanid;
    }

    public void setYuanid(long yuanid) {
        this.yuanid = yuanid;
    }


    public String getYuanname() {
        return yuanname;
    }

    public void setYuanname(String yuanname) {
        this.yuanname = yuanname;
    }


    public long getYuancount() {
        return yuancount;
    }

    public void setYuancount(long yuancount) {
        this.yuancount = yuancount;
    }

    @Override
    public String toString() {
        return "YuanTable{" +
                "yuanid=" + yuanid +
                ", yuanname='" + yuanname + '\'' +
                ", yuancount=" + yuancount +
                '}';
    }
}
