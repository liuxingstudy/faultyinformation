package cn.edu.gues.pojo;


public class SalaryTable {

    private long salaryid;
    private double basesalary;
    private double bonus;
    private double avgsalary;
    private java.sql.Timestamp ffdate;


    public long getSalaryid() {
        return salaryid;
    }

    public void setSalaryid(long salaryid) {
        this.salaryid = salaryid;
    }


    public double getBasesalary() {
        return basesalary;
    }

    public void setBasesalary(double basesalary) {
        this.basesalary = basesalary;
    }


    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }


    public double getAvgsalary() {
        return avgsalary;
    }

    public void setAvgsalary(double avgsalary) {
        this.avgsalary = avgsalary;
    }


    public java.sql.Timestamp getFfdate() {
        return ffdate;
    }

    public void setFfdate(java.sql.Timestamp ffdate) {
        this.ffdate = ffdate;
    }

    @Override
    public String toString() {
        return "SalaryTable{" +
                "salaryid=" + salaryid +
                ", basesalary=" + basesalary +
                ", bonus=" + bonus +
                ", avgsalary=" + avgsalary +
                ", ffdate=" + ffdate +
                '}';
    }
}
