package cn.edu.gues.pojo;


public class JcTable {

    private long jcid;
    private java.sql.Timestamp jctime;
    private String jcyy;
    private String jcaddress;


    public long getJcid() {
        return jcid;
    }

    public void setJcid(long jcid) {
        this.jcid = jcid;
    }


    public java.sql.Timestamp getJctime() {
        return jctime;
    }

    public void setJctime(java.sql.Timestamp jctime) {
        this.jctime = jctime;
    }


    public String getJcyy() {
        return jcyy;
    }

    public void setJcyy(String jcyy) {
        this.jcyy = jcyy;
    }


    public String getJcaddress() {
        return jcaddress;
    }

    public void setJcaddress(String jcaddress) {
        this.jcaddress = jcaddress;
    }

    @Override
    public String toString() {
        return "JcTable{" +
                "jcid=" + jcid +
                ", jctime=" + jctime +
                ", jcyy='" + jcyy + '\'' +
                ", jcaddress='" + jcaddress + '\'' +
                '}';
    }
}
