package cn.edu.gues;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FaultyInformationApplication {
    public static void main(String[] args) {
        SpringApplication.run(FaultyInformationApplication.class, args);
    }

}
