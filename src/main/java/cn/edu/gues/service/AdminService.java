package cn.edu.gues.service;

import cn.edu.gues.pojo.FaultyTable;
import cn.edu.gues.pojo.UserTable;

import java.util.List;

public interface AdminService {
    UserTable adminLogin(UserTable userTable);

    List<FaultyTable> adminList();

    int changePass(UserTable userTable);

    FaultyTable selectUserById(int faultyid);

    void fildIdDel(int faultyid);

    int faultyAdd(FaultyTable faultyTable);
}


