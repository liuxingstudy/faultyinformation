package cn.edu.gues.service.Impl;

import cn.edu.gues.mapper.AdminMapper;
import cn.edu.gues.pojo.FaultyTable;
import cn.edu.gues.pojo.UserTable;
import cn.edu.gues.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;

    @Override
    public UserTable adminLogin(UserTable userTable) {
        return adminMapper.adminLogin(userTable);
    }

    @Override
    public List<FaultyTable> adminList() {
        return adminMapper.adminList();
    }

    @Override
    public int changePass(UserTable userTable) {
        return adminMapper.changePass(userTable);
    }

    @Override
    public FaultyTable selectUserById(int faultyid) {
        return adminMapper.selectUserById(faultyid);
    }

    @Override
    public void fildIdDel(int faultyid) {
        adminMapper.fildIdDel(faultyid);
    }

    @Override
    public int faultyAdd(FaultyTable faultyTable) {
        return adminMapper.faultyAdd(faultyTable);
    }


}
