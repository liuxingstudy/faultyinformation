/*
 Navicat Premium Data Transfer

 Source Server         : TY_MySQL
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : faulty

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 01/07/2020 18:42:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for faulty_table
-- ----------------------------
DROP TABLE IF EXISTS `faulty_table`;
CREATE TABLE `faulty_table`  (
  `faultyid` int(0) NOT NULL COMMENT '教职工编号',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `work` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职称',
  `age` int(0) NOT NULL COMMENT '年龄',
  `sex` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '性别',
  `nation` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名族',
  `zzmm` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '政治面貌',
  `birth` date NOT NULL COMMENT '出生日期',
  `xueli` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学历',
  `hun` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '婚姻状态',
  PRIMARY KEY (`faultyid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of faulty_table
-- ----------------------------
INSERT INTO `faulty_table` VALUES (1, '张继燕', '14', 45, '女', '汉', '党员', '2020-06-18', '研究生', '已婚');
INSERT INTO `faulty_table` VALUES (2, '娄必伟', '20', 50, '男', '汉', '党员', '2020-06-18', '研究生', '已婚');

-- ----------------------------
-- Table structure for jc_table
-- ----------------------------
DROP TABLE IF EXISTS `jc_table`;
CREATE TABLE `jc_table`  (
  `jcid` int(0) NOT NULL COMMENT '奖惩编号',
  `jctime` datetime(6) NOT NULL COMMENT '奖惩时间',
  `jcyy` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '奖惩原因',
  `jcaddress` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '奖惩地点',
  PRIMARY KEY (`jcid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jc_table
-- ----------------------------

-- ----------------------------
-- Table structure for salary_table
-- ----------------------------
DROP TABLE IF EXISTS `salary_table`;
CREATE TABLE `salary_table`  (
  `salaryid` int(0) NOT NULL COMMENT '薪资编号',
  `basesalary` decimal(10, 2) NOT NULL COMMENT '基本工资',
  `bonus` decimal(10, 0) NOT NULL COMMENT '奖金',
  `avgsalary` decimal(10, 2) NOT NULL COMMENT '实发工资',
  `ffdate` datetime(6) NOT NULL COMMENT '发放日期',
  PRIMARY KEY (`salaryid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of salary_table
-- ----------------------------

-- ----------------------------
-- Table structure for user_table
-- ----------------------------
DROP TABLE IF EXISTS `user_table`;
CREATE TABLE `user_table`  (
  `userid` int(0) NOT NULL COMMENT '用户id',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `pass` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `role` int(0) NOT NULL COMMENT '1：表示管理员  0：表示用户',
  PRIMARY KEY (`userid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_table
-- ----------------------------
INSERT INTO `user_table` VALUES (1, 'user', '123', 0);
INSERT INTO `user_table` VALUES (2, 'admin', '123', 1);

-- ----------------------------
-- Table structure for yuan_table
-- ----------------------------
DROP TABLE IF EXISTS `yuan_table`;
CREATE TABLE `yuan_table`  (
  `yuanid` int(0) NOT NULL COMMENT '院的编号',
  `yuanname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '院的名称',
  `yuancount` int(0) NOT NULL COMMENT '院的人数',
  PRIMARY KEY (`yuanid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yuan_table
-- ----------------------------
INSERT INTO `yuan_table` VALUES (1, '信工院', 360);
INSERT INTO `yuan_table` VALUES (2, '理学院', 400);

SET FOREIGN_KEY_CHECKS = 1;
